﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideController : MonoBehaviour {

    [Range(1f, 2f)]
    public float MovementRatio = 1.3f;

    private bool _touching;
    private Vector2? _deltaPosition;
#if UNITY_EDITOR
    private Vector2? _lastposition =new Vector2(0, 0);
#endif


    private void Start()
    {
        _touching = false;

    }

    private void Update()
    {
        _touching = Input.touchCount > 0 || Input.GetMouseButton(0);

        if (_touching)
        {
#if UNITY_EDITOR
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _deltaPosition = mousePosition - _lastposition;
            _lastposition = mousePosition;
#else
            _deltaPosition = Input.GetTouch(0).deltaPosition;
#endif
            ComputeMovement();
        }
        else
            _deltaPosition = null;

    }

    private void ComputeMovement()
    {
        Vector2 position = transform.position;
        position.x += _deltaPosition.Value.x * MovementRatio;
        transform.position = position;
    }
}
